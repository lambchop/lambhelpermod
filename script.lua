local VERSION = 1.0

if lambHelperMod then -- adopted from piber20helper
	local thisVersion = 1
	if lambHelperMod.VERSION then
		thisVersion = lambHelperMod.VERSION
	end
	if thisVersion < VERSION then
		lambHelperMod = nil
		print("[lambHelperMod] Replacing older scripts...")
	end
end

if not lambHelperMod then
	lambHelperMod = RegisterMod("lambhelpermod", 1)
	lambHelperMod.MODNAME = "[lambHelperMod]"
	lambHelperMod.VERSION = 1.0
	lambHelperMod.LOADED = false
	print(lambHelperMod.MODNAME, "Loading script...")
	lambHelperMod.PATHPREFIX = "scripts/lambhelper/resources/"
	lambHelperMod.GAME = Game()
	lambHelperMod.SFX = SFXManager()
	lambHelperMod.RNG = RNG()
	
	lambHelperMod:AddCallback(ModCallbacks.MC_POST_GAME_STARTED, function(_, isSave)
		if isSave == true then
		else
			lambHelperMod.RNG:SetSeed(lambHelperMod.GAME:GetSeeds():GetStartSeed(), 35)
		end
	end)

	lambHelperMod.Colors = {
		COLOR_HOMING = Color(0.4, 0.15, 0.38, 1, 44, 2, 74),
		COLOR_GHOST = Color(0.5, 0.5, 0.5, 0.5, 200, 200, 200)
	}
	
	lambHelperMod.AchPNG = "None"
	lambHelperMod.AchPaperPNG = "None"
	lambHelperMod.AchSND = 0
	lambHelperMod.IsShowingAchievement = false
	
	lambHelperMod.UiSprite = {
		Ach = Sprite(),
	}
	
	lambHelperMod.UiSprite.Ach:Load(lambHelperMod.PATHPREFIX .. "gfx/ui/achievements.anm2", true)
	
	local JustShowed = nil
	local wasCleared = false

	lambHelperMod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function(_)
		lambHelperMod.ROOM = lambHelperMod.GAME:GetRoom()
		if not lambHelperMod.ROOM:IsClear() then
			wasCleared = false
		end
	end)
	
	lambHelperMod:AddCallback(ModCallbacks.MC_POST_NEW_LEVEL, function(_)
		lambHelperMod.LEVEL = lambHelperMod.GAME:GetLevel()
	end)
	
	function lambHelperMod.ForceBloodTear(tear)
		if tear.Variant == TearVariant.BLUE then
			tear:ChangeVariant(TearVariant.BLOOD)
		elseif tear.Variant == TearVariant.NAIL then
			tear:ChangeVariant(TearVariant.NAIL_BLOOD)
		elseif tear.Variant == TearVariant.GLAUCOMA then
			tear:ChangeVariant(TearVariant.GLAUCOMA_BLOOD)
		elseif tear.Variant == TearVariant.CUPID_BLUE then
			tear:ChangeVariant(TearVariant.CUPID_BLOOD)
		elseif tear.Variant == TearVariant.EYE then
			tear:ChangeVariant(TearVariant.EYE_BLOOD)
		elseif tear.Variant == TearVariant.PUPULA then
			tear:ChangeVariant(TearVariant.PUPULA_BLOOD)
		elseif tear.Variant == TearVariant.GODS_FLESH then
			tear:ChangeVariant(TearVariant.GODS_FLESH_BLOOD)
		end
	end
	
	function lambHelperMod.GetPercentage(Num1, Num2, Round, Invert) -- adopted from cucco's ioi
		local Percentage = (Num1 / Num2) * 100
		Percentage = (math.floor(Percentage*100)/100)
		if Round then
			Percentage = math.floor(Percentage)
		end
		if Invert then
			Percentage = math.abs(Percentage-100)
		end
		return Percentage
	end
	
	function lambHelperMod.DestroyNearbyGrid(e, radius)
		local radius = radius or 10
		for i = 0, (lambHelperMod.ROOM:GetGridSize()) do
			local gent = lambHelperMod.ROOM:GetGridEntity(i)
			if lambHelperMod.ROOM:GetGridEntity(i) then
				if (e.Position - gent.Position):Length() <= radius then
					gent:Destroy()
				end
			end
		end
	end
	
	function lambHelperMod.ReplacePlayerSpritesheet(player, sheet)
		local sprite = player:GetSprite()
		
		sprite:ReplaceSpritesheet(12, sheet)
		sprite:ReplaceSpritesheet(4, sheet)
		sprite:ReplaceSpritesheet(2, sheet)
		sprite:ReplaceSpritesheet(1, sheet)
		
		sprite:LoadGraphics()
		sprite:Update()
	end
	
	function lambHelperMod.GetScreenCenterPos() -- adopted from piber20helper
		if not lambHelperMod.ROOM then lambHelperMod.ROOM = lambHelperMod.GAME:GetRoom() end
		local shape = lambHelperMod.ROOM:GetRoomShape()
		local centerOffset = lambHelperMod.ROOM:GetCenterPos() - lambHelperMod.ROOM:GetTopLeftPos()
		
		local pos = lambHelperMod.ROOM:GetCenterPos()
		
		if centerOffset.X > 260 then
			pos.X = pos.X - 260
		end
		if shape == RoomShape.ROOMSHAPE_LBL or shape == RoomShape.ROOMSHAPE_LTL then
			pos.X = pos.X - 260
		end
		if centerOffset.Y > 140 then
			pos.Y = pos.Y - 140
		end
		if shape == RoomShape.ROOMSHAPE_LTR or shape == RoomShape.ROOMSHAPE_LTL then
			pos.Y = pos.Y - 140
		end
		
		return Isaac.WorldToRenderPosition(pos, false)
	end
	
	function lambHelperMod.GetPlayers(functionCheck, ...) -- adopted from piber20helper
		local args = {...}
		local players = {}
		for i=1, lambHelperMod.GAME:GetNumPlayers() do
			local player = Isaac.GetPlayer(i-1)
			local argsPassed = true
			if type(functionCheck) == "function" then
				for j=1, #args do
					if args[j] == "player" then
						args[j] = player
					elseif args[j] == "currentPlayer" then
						args[j] = i
					end
				end
				if not functionCheck(table.unpack(args)) then
					argsPassed = false
				end
			end
			if argsPassed then
				players[#players+1] = player
			end
		end
		return players
	end
	
	function lambHelperMod.GetPlayerUsingItem(strict) -- adopted from piber20helper
		local player = nil
		
		for _, thisPlayer in pairs(lambHelperMod.GetPlayers()) do
			if Input.IsActionTriggered(ButtonAction.ACTION_ITEM, thisPlayer.ControllerIndex) or Input.IsActionTriggered(ButtonAction.ACTION_PILLCARD, thisPlayer.ControllerIndex) then
				player = thisPlayer
				break
			end
		end
		
		if not strict and player == nil then
			player = Isaac.GetPlayer(0)
		end
		
		return player
	end
	
	function lambHelperMod.ShowCustomAchievement(achievementPNG, paperPNG, sound)
		if lambHelperMod.IsShowingAchievement == false then
			lambHelperMod.AchPNG = achievementPNG
			lambHelperMod.AchPaperPNG = paperPNG
			lambHelperMod.AchSND = sound
			lambHelperMod.IsShowingAchievement = true
			JustShowed = false
		end
	end
	
	lambHelperMod:AddCallback(ModCallbacks.MC_POST_FIRE_TEAR, function(_, tear)
		if tear.Parent.Type == EntityType.ENTITY_PLAYER then
			local player = tear.Parent:ToPlayer()
			local pdata = player:GetData()
			local tdata = tear:GetData()
			
			if pdata.lambdata_firedtears == nil then pdata.lambdata_firedtears = 1 end
			
			if pdata.lambdata_firedtears == 0 then
				if tdata.lambdata_islefteye == nil then tdata.lambdata_islefteye = false end
				tdata.lambdata_islefteye = true
			end
			
			pdata.lambdata_firedtears = (pdata.lambdata_firedtears + 1) % 2
		end
	end)
	
	lambHelperMod:AddCallback(ModCallbacks.MC_POST_UPDATE, function(_)	
		if lambHelperMod.ROOM:IsClear() and wasCleared == false then
			-- code goes here
			wasCleared = true
		end
		
		if StageAPI and wasCleared == false then
			local currentRoom = StageAPI.GetCurrentRoom()
			if currentRoom then
				local isClear = currentRoom.IsClear
				currentRoom.IsClear = lambHelperMod.ROOM:IsClear()
				if not isClear and currentRoom.IsClear then
					-- stageapi support
					wasCleared = true
				end
			end
		end
	end)
	
	lambHelperMod:AddCallback(ModCallbacks.MC_POST_RENDER, function(_)
		if lambHelperMod.IsShowingAchievement == true then
			lambHelperMod.UiSprite.Ach:Update()
			if not JustShowed then
				if type(lambHelperMod.AchPNG) ~= "string" then lambHelperMod.AchPNG = lambHelperMod.PATHPREFIX .. "gfx/ui/achievement_na.png" end
				if type(lambHelperMod.AchPaperPNG) ~= "string" then lambHelperMod.AchPaperPNG = lambHelperMod.PATHPREFIX .. "gfx/ui/paper.png" end
				lambHelperMod.UiSprite.Ach:ReplaceSpritesheet(0, lambHelperMod.AchPaperPNG)
				lambHelperMod.UiSprite.Ach:ReplaceSpritesheet(1, lambHelperMod.AchPNG)
				lambHelperMod.UiSprite.Ach:LoadGraphics()
				lambHelperMod.UiSprite.Ach:Play("Appear", false)
				JustShowed = true
			end
			
			if lambHelperMod.UiSprite.Ach:GetFrame() == 0 and lambHelperMod.UiSprite.Ach:IsPlaying("Appear") then
				if type(lambHelperMod.AchSND) ~= "number" then lambHelperMod.AchSND = SoundEffect.SOUND_BOOK_PAGE_TURN_12 end
				lambHelperMod.SFX:Play(lambHelperMod.AchSND, 1, 0, false, 1)
			end
			
			if lambHelperMod.UiSprite.Ach:IsFinished("Appear") then
				lambHelperMod.UiSprite.Ach:Play("Idle", true)
			elseif lambHelperMod.UiSprite.Ach:IsFinished("Idle") then
				lambHelperMod.UiSprite.Ach:Play("Disappear", true)
			elseif lambHelperMod.UiSprite.Ach:IsFinished("Disappear") then
				lambHelperMod.IsShowingAchievement = false
			end
			
		end
		lambHelperMod.UiSprite.Ach:Render(Vector(lambHelperMod.GetScreenCenterPos().X, (lambHelperMod.GetScreenCenterPos().Y) * 0.4), Vector(0, 0), Vector(0, 0))
	end)
	
	lambHelperMod.LOADED = true
	print(lambHelperMod.MODNAME, "Loaded successfully.", "Version:", lambHelperMod.VERSION)
end